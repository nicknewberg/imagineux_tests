﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

namespace Test
{
    public class OpenDMXTest : MonoBehaviour
    {
        bool error = false;

        // Use this for initialization
        void Start()
        {
            try
            {
                OpenDMX.start();                                            //find and connect to devive (first found if multiple)
                if (OpenDMX.status == FT_STATUS.FT_DEVICE_NOT_FOUND) {
                    Debug.Log("No Enttec USB Device Found");
                    error = true;
                }
                else if (OpenDMX.status == FT_STATUS.FT_OK)
                    Debug.Log("Found DMX on USB");
                else
                {
                    Debug.Log("Error Opening Device");
                    error = true;
                }
                    
            }
            catch (Exception exp)
            {
                Debug.Log("Error Connecting to Enttec USB Device");
                error = true;
            }


            if (!error)
            {
                OpenDMX.setDmxValue(1, 100);
                //OpenDMX.setDmxValue(2, 100);
                //OpenDMX.setDmxValue(3, 100);
                //OpenDMX.writeData();
            }
            

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

