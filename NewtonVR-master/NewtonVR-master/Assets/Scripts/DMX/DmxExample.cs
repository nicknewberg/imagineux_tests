﻿using UnityEngine;
using System.Collections;
using ETC.Platforms;

public class DmxExample : MonoBehaviour
{
    private DMX dmx;


    void Start()
    {
        this.dmx = new DMX("COM5");
        this.dmx.Channels[1] = 50;
        //this.dmx.Channels[2] = 50;
        //this.dmx.Channels[3] = 50;
        this.dmx.Send();
    }

    void Update()
    {
        this.dmx.Channels[1] = 50;
        this.dmx.Send();
    }
}
