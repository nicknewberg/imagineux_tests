﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class PianoKey : MonoBehaviour
    {
        public Rigidbody Rigidbody;

        [Tooltip("The (worldspace) distance from the initial position you have to push the button for it to register as pushed")]
        public float DistanceToEngage = 0.075f;

        [Tooltip("Is set to true when the button has been pressed down this update frame")]
        public bool ButtonDown = false;

        [Tooltip("Is set to true when the button has been released from the down position this update frame")]
        public bool ButtonUp = false;

        [Tooltip("Is set to true each frame the button is pressed down")]
        public bool ButtonIsPushed = false;

        [Tooltip("Is set to true if the button was in a pushed state last frame")]
        public bool ButtonWasPushed = false;

        private bool ButtonAtRest = false; //is true when the button is at its resting (initial) position and has no velocity

        public Material initKeyMat; //initial unpressed key material
        public Material pressedKeyMat; //'highlight' material when key is pressed
        public AudioSource audioSource; //the audio source for this key (each key has its own)
        public ParticleSystem butterflySystem; //the butterfly particle system (each key has its own)

        protected Transform InitialPosition;
        protected float MinDistance = 0.001f;
        protected float MaxDistance = 0.015f;

        protected float PositionMagic = 500f;

        protected float CurrentDistance = -1;

        private void Awake()
        {
            InitialPosition = new GameObject(string.Format("[{0}] Initial Position", this.gameObject.name)).transform;
            InitialPosition.parent = this.transform.parent;
            InitialPosition.localPosition = Vector3.zero;
            InitialPosition.localRotation = Quaternion.identity;

            if (Rigidbody == null)
                Rigidbody = this.GetComponent<Rigidbody>();

            if (Rigidbody == null)
            {
                Debug.LogError("There is no rigidbody attached to this button.");
            }

        }

        private void FixedUpdate()
        {
            CurrentDistance = Vector3.Distance(this.transform.position, InitialPosition.position);

            if (CurrentDistance > MinDistance)
            {
                ButtonAtRest = false;
                Vector3 PositionDelta = InitialPosition.position - this.transform.position;
                this.Rigidbody.velocity = PositionDelta * PositionMagic * Time.fixedDeltaTime;
            }else
            {
                this.Rigidbody.velocity = Vector3.zero;
                ButtonAtRest = true;

                ////if the key is at rest, stop the sound and the butterflies
                //audioSource.Stop();
                //butterflySystem.Pause();                
                ////if (butterflySystem.isPlaying)
                ////{
                ////    print("STOP");
                ////    butterflySystem.Stop();

                ////}


            }
        }

        private void Update()
        {
            //stop key from being pushed up from the bottom
            if (transform.position.y > InitialPosition.position.y)
            {
                this.transform.position = InitialPosition.position;
            }

            //if the button is at rest, stop any audio and butterfly emission
            if (ButtonAtRest)
            {
                audioSource.Stop();
                butterflySystem.enableEmission = false;
                butterflySystem.Stop();

            }

            ButtonWasPushed = ButtonIsPushed;
            ButtonIsPushed = CurrentDistance > DistanceToEngage;

            if (ButtonWasPushed == false && ButtonIsPushed == true)
            {
                ButtonDown = true;
                
                //change color,  play sound and trigger butterflies if the button is down
                GetComponent<Renderer>().material = pressedKeyMat;
                audioSource.Play();
                butterflySystem.enableEmission = true;
                int numButterflies = Mathf.RoundToInt(Random.Range(1f, 3f));
                butterflySystem.Emit(numButterflies);
                butterflySystem.Play();


                //if (!butterflySystem.isPlaying)
                //{
                //    butterflySystem.Emit(5);
                //}
            }
            else
                ButtonDown = false;

            if (ButtonWasPushed == true && ButtonIsPushed == false)
            {
                ButtonUp = true;
                GetComponent<Renderer>().material = initKeyMat;

            }
            else
                ButtonUp = false;
        }
    }
}