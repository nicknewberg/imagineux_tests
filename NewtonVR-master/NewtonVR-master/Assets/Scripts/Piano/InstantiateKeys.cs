﻿using UnityEngine;
using System.Collections;

public class InstantiateKeys : MonoBehaviour {

    public GameObject prefab;

	// Use this for initialization
	void Start () {

        Vector3 currPos = this.transform.position;
        float currPitch = 0.3f;

        for (int i = 0; i < 36; i++)
        {
            currPos = currPos + new Vector3(0f, 0f, 0.0315f);
            currPitch = currPitch + 0.05f;
            prefab.GetComponentInChildren<AudioSource>().pitch = currPitch;
            GameObject.Instantiate(prefab, currPos, this.transform.rotation);
            
        }
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
