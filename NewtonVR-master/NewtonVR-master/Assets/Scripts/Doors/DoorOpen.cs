﻿using UnityEngine;
using System.Collections;

public class DoorOpen : MonoBehaviour {

    public Transform head; //player head transform
    public Transform fullOpenTrigger;//point at which door is open (90 degrees)

    private Quaternion openRot; 
    private Quaternion closeRot;

    private bool throughDoor; //did the user walk through the door

	// Use this for initialization
	void Start () {
        openRot = Quaternion.Euler(new Vector3(0f, 90f, 0f));
        closeRot = Quaternion.Euler(Vector3.zero);
        throughDoor = false;
	}
	
	// Update is called once per frame
	void Update () {
        float distance = Vector3.Distance(fullOpenTrigger.position, head.position);

        float relativeDistance = head.position.x - fullOpenTrigger.position.x;

        if (relativeDistance < 0f) // we are through the door
        {
            throughDoor = true;
        }else 
        {
            throughDoor = false;
        }

        if (distance <= 1f && distance >= 0f && !throughDoor) //if within opening range and have not entered the door, 
            //rotate door proportional to distance
        {
            Vector3 eulerRot = Vector3.Lerp(openRot.eulerAngles, closeRot.eulerAngles, distance);
            this.transform.localRotation = Quaternion.Euler(eulerRot);
        }

	}
}
