﻿using UnityEngine;
using System.Collections;

public class DoorOpenStepped : MonoBehaviour {

    public Transform head; //player head transform
    public Transform doorLocation;

    private Quaternion fullOpenRot; //fully open rotation
    private Quaternion crackOpenRot; //cracked open rotation
    private Quaternion closeRot; //fully closed rotation
    private Quaternion correctionTargetRot; //rotation for correction <Temp>

    private bool throughDoor; //did the user walk through the door
    private bool cracking; //is the door in the process of cracking
    private bool closing; //is the door in the process of closing
    private bool correcting; //is the door correcting its position (EDGE CASE)
    private bool opened; //is the door fully opened
    private bool opening; //is the door opening fully
    private bool inFocus; //is this the door of intent

    // Use this for initialization
    void Start()
    {
        fullOpenRot = Quaternion.Euler(new Vector3(0f, 90f, 0f));
        crackOpenRot = Quaternion.Euler(new Vector3(0f, 18f, 0f));
        closeRot = Quaternion.Euler(Vector3.zero);
        throughDoor = false;
        cracking = false;
        closing = false;
        correcting = false; 
        opened = false;
        opening = false;
        inFocus = false;
    }

    // Update is called once per frame
    void Update()
    {
        float distFromDoor = Vector3.Distance(doorLocation.position, head.position);

        if (distFromDoor <= 1f && distFromDoor >= 0f && inFocus && !correcting)
        {
            opening = true;
            Vector3 eulerRot = Vector3.Lerp(fullOpenRot.eulerAngles, crackOpenRot.eulerAngles, distFromDoor);

            if (Mathf.Abs(eulerRot.y - this.transform.localEulerAngles.y) >= 10f && !correcting)
            {
                correctionTargetRot = Quaternion.Euler(eulerRot);
                correcting = true;
                cracking = false;
                closing = false;
            }

            if (!correcting)
            {
                this.transform.localRotation = Quaternion.Euler(eulerRot);
            }
            
        }else
        {
            opening = false;
            if (cracking)
            {
                //print("cracking");
                this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, crackOpenRot, Time.deltaTime * 2f);
                inFocus = true;
            }
            else if (closing)
            {
                //print("closing");
                this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, closeRot, Time.deltaTime * 3f);
                inFocus = false;
            } else if (correcting)
            {
                //print("correcting1");
                this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, correctionTargetRot, Time.deltaTime *3f);
                if (Mathf.Abs(this.transform.localEulerAngles.y - correctionTargetRot.eulerAngles.y) < 2f)
                {
                    correcting = false;
                }
            }
        }


    }

    public void crack()
    {
        if (!correcting)
        {
            closing = false;
            cracking = true;
        }
    }

    public void close()
    {
        if (!correcting)
        {
            cracking = false;
            closing = true;
        }
    }

    public bool isOpening()
    {
        return opening;
    }

    public void focus()
    {
        inFocus = true;
    }

    public void removeFocus()
    {
        inFocus = false;
    }

    public bool isCorrecting()
    {
        return correcting;
    }

}
