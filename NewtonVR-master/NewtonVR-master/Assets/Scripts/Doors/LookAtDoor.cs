﻿using UnityEngine;
using System.Collections;

public class LookAtDoor : MonoBehaviour
{

    private DoorOpenStepped currDoor;
    private bool tryingToEnterDoor;
    private bool doorIsCorrecting;

    // Use this for initialization
    void Start()
    {
        tryingToEnterDoor = false;
        doorIsCorrecting = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (currDoor != null) // a person is trying to enter a door when that door is opening
        {
            tryingToEnterDoor = currDoor.isOpening();
            doorIsCorrecting = currDoor.isCorrecting();
        }

        if (!tryingToEnterDoor && !doorIsCorrecting) //Only open and close other doors when we aren't trying to enter another one and a door isn't correcting
        {
            RaycastHit hit;

            if (Physics.Raycast(transform.position, transform.forward, out hit))
            {
                if (hit.collider.CompareTag("door")) //if we are looking at a door, crack it open
                {
                    DoorOpenStepped newDoor = hit.collider.GetComponent<DoorOpenStepped>();

                    if (newDoor != currDoor) //if its a new door  crack it and close the old one
                    {
                        if (currDoor != null)
                        {
                            currDoor.close();
                        }

                        currDoor = newDoor;
                        currDoor.crack();
                    }
                }
            }else if (currDoor != null) //if we aren't looking at a door, close the last one we did
            {
                currDoor.close();
                currDoor = null;
            }
        }else
        {
            currDoor.focus();
        }
    }
}
