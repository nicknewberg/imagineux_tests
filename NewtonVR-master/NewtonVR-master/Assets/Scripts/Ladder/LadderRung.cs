﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class LadderRung : NVRInteractableItem
    {
        public GameObject player;
        private bool isClimbing = false;
        private Vector3 attachPoint;
        public OutlineOscillate Outline;

        protected override void Update()
        {
            base.Update();

            if (isClimbing)
            {
                //float deltaY = this.transform.position.y - this.AttachedHand.transform.position.y;
                //player.transform.position += new Vector3(0f, deltaY, 0f);

                Vector3 deltaPos = this.attachPoint - this.AttachedHand.transform.position;
                player.transform.position += new Vector3(0f, deltaPos.y, deltaPos.z);
            }
               
        }

        public override void BeginInteraction(NVRHand hand)
        {
            base.BeginInteraction(hand);
            attachPoint = this.AttachedHand.transform.position;
            isClimbing = true;
            Ladder.highlightNext();

        }

        public override void EndInteraction()
        {
            base.EndInteraction();
            isClimbing = false;

        }
    }
 }