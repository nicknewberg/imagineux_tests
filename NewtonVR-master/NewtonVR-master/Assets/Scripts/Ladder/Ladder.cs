﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class Ladder : MonoBehaviour
    {

        public static LadderRung[] rungs;
        private static int currRung; //index of current rung

        // Use this for initialization
        void Start()
        {
            rungs = new LadderRung[5];
            for (int i = 0; i < rungs.Length; i++)
            {
                rungs[i] = GameObject.Find("Rung" + i.ToString()).GetComponent<LadderRung>();
            }

            currRung = 1;

            highlightNext();

        }

        public static void highlightNext()
        {
            if (currRung < rungs.Length)
            {
                rungs[currRung].Outline.off();
                currRung++;
                rungs[currRung].Outline.on();
            }
        }

    }
}

