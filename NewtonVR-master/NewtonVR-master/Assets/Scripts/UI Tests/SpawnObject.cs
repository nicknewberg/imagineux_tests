﻿using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour {

    public bool isKinematic = true;
    public bool isSpawning = false;
    private Vector3 initScale;
    public Vector3 maxScale;
    private Vector3 initPos;
    private Vector3 initLocalPos;
    private bool wroteAttachedZ = false;
    private float attachedZ;
    private bool hasHitFloor = false;
    public bool isFalling = false;

	// Use this for initialization
	void Start () {
        initPos = this.transform.position;
        initLocalPos = this.transform.localPosition;
        initScale = this.transform.localScale;

	}
	
	// Update is calle d once per frame
	void Update () {

        if (isSpawning)
        {
            if (!wroteAttachedZ)
            {
                attachedZ = this.transform.localPosition.z;
                wroteAttachedZ = true;
            }

            //Debug.Log(Vector3.Distance(this.transform.position, initPos));
            if (!isFalling)
            {
                float t = Mathf.Clamp(Vector3.Distance(this.transform.position, initPos), 0f, 2f) / 2f;
                //Debug.Log(t);

                //this.transform.localPosition += 2f * Time.deltaTime * Vector3.forward;
                this.transform.localScale = Vector3.Lerp(initScale, maxScale, t);
                Vector3 minLocalPos = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y, attachedZ);
                Vector3 maxLocalPos = minLocalPos + 1f * Vector3.forward;
                this.transform.localPosition = Vector3.Lerp(minLocalPos, maxLocalPos, t);
            }
            else if (hasHitFloor || isKinematic)
            {
                this.transform.localScale = Vector3.Lerp(this.transform.localScale, maxScale, Time.deltaTime*1.5f);
            }

            if(nearlyEqual(this.transform.localScale, maxScale))
            {
                isSpawning = false;
                hasHitFloor = false;
            }

        }



    }

    void OnCollisionEnter(Collision collision)
    {
        if(!collision.collider.CompareTag("PaletteElement")
            && !collision.collider.CompareTag("Scroll"))
            hasHitFloor = true;
    }

    public void InitSpawn()
    {
        initPos = this.transform.position;
        isSpawning = true;
    }

    private bool nearlyOne(float num)
    {
        return (1 - num) <= 0.001f;
    }

    private bool nearlyEqual(Vector3 a, Vector3 b)
    {
        return Vector3.Distance(a, b) < 0.001f;
    }
}
