﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Handles the choosing of elements,
//spawning and manipulation of objects in the scene
//Uses the Caster to figure out what we are pointing at
//Change class name to InteractionController
public class ElementSpawner : MonoBehaviour {

    public NewtonVR.NVRHand Hand;
    public Transform Head;
    public Palette Palette;
    private UICaster Caster;
    private bool isGrabbing = false;
    private GameObject selectedObject;

    void Start()
    {
        Caster = this.GetComponent<UICaster>();
    }

	// Update is called once per frame
	void Update () {


	    if (Caster.isHighlightingPalette && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger].PressDown) //Selecting Palette Element so Spawn new Object
        {
            Vector3 objectPos = new Vector3(Head.transform.position.x + Head.transform.forward.x* 2f, 0f ,Head.transform.position.z
                + Head.transform.forward.z * 2f) + 0.5f*Vector3.up;
            GameObject.Instantiate(Caster.currHighlighted.associatedObject, objectPos , Quaternion.identity);
        }else if (Caster.isHighlightingObject) //if we are pointing at an object (not a pallete element)
        {

            if (!isGrabbing && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger].IsPressed) //Grab the object if we are pulling the trigger
            {
                selectedObject = Caster.currSelection.gameObject;
                isGrabbing = true;
                selectedObject.transform.parent = Hand.transform;
                Rigidbody grabObjectRigid = selectedObject.GetComponent<Rigidbody>();
                if (grabObjectRigid)
                {
                    grabObjectRigid.isKinematic = true;
                }
            }

        }

         if (isGrabbing && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger].PressUp) //Drop the object if we just let go of the trigger
        {
            //var grabObject = Caster.currSelection;
            isGrabbing = false;
            selectedObject.transform.parent = null;
            Rigidbody selectedObjectRigid = selectedObject.GetComponent<Rigidbody>();
            if (selectedObjectRigid)
            {
                selectedObjectRigid.isKinematic = false;
                selectedObjectRigid.AddForce(Hand.GetVelocityEstimation() * -25f, ForceMode.Impulse);
            }

        }

        if (Caster.isHighlightingScroll && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger].PressDown)
        {
            //Scroll Palette
            Palette.Scroll();
            Debug.Log("Scroll");

        }

        if(isGrabbing && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0].SingleAxis > 0.25f)
        {
            selectedObject.transform.eulerAngles += 100f*Time.deltaTime*Vector3.up;
        }else if (isGrabbing && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0].SingleAxis < -0.25f)
        {
            selectedObject.transform.eulerAngles -= 100f * Time.deltaTime * Vector3.up;
        }else if (isGrabbing && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0].Axis.y > 0.25f)
        {
            selectedObject.transform.localPosition += 2f * Time.deltaTime * Vector3.forward;
        }else if (isGrabbing && Hand.Inputs[Valve.VR.EVRButtonId.k_EButton_Axis0].Axis.y < -0.25f)
        {
            selectedObject.transform.localPosition -= 2f * Time.deltaTime * Vector3.forward;
        }
    }
}
