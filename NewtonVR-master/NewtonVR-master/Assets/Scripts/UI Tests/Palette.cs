﻿using UnityEngine;
using System.Collections;

public class Palette : MonoBehaviour {

    private Quaternion targetRotation;
    private Vector3 targetPosition;
    private bool isScrolling = false;
    private float distBetweenElements = -1.15f;
    public Transform startPos, startPage;
    private Vector3 currPos;
    public PaletteElement[] PaletteElements;
    // Use this for initialization
    void Start() {

        currPos = startPos.transform.localPosition;
        int currElement = 0;
        int pages = Mathf.RoundToInt(PaletteElements.Length / 9);
        for (int currPage = 0; currPage < pages; currPage++)
        {
            currPos = startPos.transform.localPosition;
            currPos.x = startPos.transform.localPosition.x - (currPage * 3 * distBetweenElements);

            for (int row = 0; row < 3; row++)
            {
                for (int col = 0; col < 3; col++)
                {
                    //Debug.Log(currElement + ", " + currPos);
                    PaletteElement element = (PaletteElement)Instantiate(PaletteElements[currElement], startPos.parent);
                    element.transform.localPosition = currPos;
                    element.transform.localRotation = PaletteElements[currElement].transform.localRotation;
                    currElement++;
                    currPos.x += distBetweenElements;
                }
                currPos.y += distBetweenElements;
                currPos.x = startPos.transform.localPosition.x - (currPage * 3 * distBetweenElements);
            }

        }


    }

    // Update is called once per frame
    void Update () {

        if (isScrolling)
        {
            this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, targetPosition, Time.deltaTime * 3f);
        }
    }

    public void Scroll()
    {
        isScrolling = true;
        targetPosition = this.transform.localPosition + Vector3.right * (0.85f);
    }
}
