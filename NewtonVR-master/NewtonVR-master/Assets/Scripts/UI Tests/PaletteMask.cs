﻿using UnityEngine;
using System.Collections;

public class PaletteMask : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("PaletteElement"))
        {
            other.GetComponent<MeshRenderer>().enabled = true;
            other.gameObject.layer = 0; //default
        }


    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("PaletteElement"))
        {
            other.GetComponent<MeshRenderer>().enabled = false;
            other.gameObject.layer = 2; //ignore raycast
        }
    }
}
