﻿using UnityEngine;
using System.Collections;

public class PaletteElement : MonoBehaviour {

    public SpawnObject associatedObject;
    public bool isSelected, isHighlighted = false; //highlight is hovering over, selected means clicked
    private Vector3 initPos, highlightedPos;


	// Use this for initialization
	void Start () {

        initPos = transform.localPosition;
        highlightedPos = initPos + new Vector3(0f, 0f, 0.40f);

        //initialize hidden so mask can show it
        this.GetComponent<MeshRenderer>().enabled = false;
        this.gameObject.layer = 2; //ignore raycast
    }
	
	// Update is called once per frame
	void Update () {

        if (isHighlighted)
        {
            this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, highlightedPos, Time.deltaTime * 5f);
        }else
        {
            this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, initPos, Time.deltaTime * 8f);
        }
	}

    public void Highlight()
    {
        if (!isHighlighted)
        {
            isHighlighted = true;
            //initPos = transform.localPosition;
            //highlightedPos = initPos + new Vector3(0f,0f,0.40f);
        }
    }

    public void UnHighlight()
    {
        isHighlighted = false;
    }
}
