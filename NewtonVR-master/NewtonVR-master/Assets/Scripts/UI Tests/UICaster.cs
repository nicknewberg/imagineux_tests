﻿using UnityEngine;
using System.Collections;
//Handles the RayCasting, Beam Display 
public class UICaster : MonoBehaviour {

    private LineRenderer line;
    private Vector3 defaultLength;
    private HapticFeedback Haptics;
    public PaletteElement currHighlighted;
    public GameObject currSelection;
    public bool isHighlightingPalette, isHighlightingObject, isHighlightingScroll = false;
    public bool paletteSelectionIsScaling = false;
    private Vector3 paletteSelectionInitPos;

    void Start()
    {
        line = this.GetComponent<LineRenderer>();
        defaultLength = new Vector3(0f, 0f, 100f);
        Haptics = this.GetComponent<HapticFeedback>();
    }
    void Update()
    {

        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            Vector3 end = new Vector3(0f,0f, transform.InverseTransformPoint(hit.transform.position).z);
            line.SetPosition(1, end);

            if (hit.collider.CompareTag("PaletteElement")) //We are highlighting a Palette Element
            {
                isHighlightingPalette = true;
                isHighlightingObject = false;
                isHighlightingScroll = false;

                PaletteElement newHighlighted = hit.collider.gameObject.GetComponent<PaletteElement>();

                if(currHighlighted != newHighlighted) //if its a new palette element
                {
                    if (currHighlighted)
                        currHighlighted.UnHighlight();

                    currHighlighted = newHighlighted;
                    currHighlighted.Highlight();
                    Haptics.Trigger();
                }

            }else if (hit.collider.CompareTag("MoveableObject")) // We are highlighting an object in the space
            {
                isHighlightingObject = true;
                isHighlightingPalette = false;
                isHighlightingScroll = false;

                currSelection = hit.collider.gameObject;
            }else if (hit.collider.CompareTag("Scroll")) //We are highlighting the scroll button
            {
                isHighlightingScroll = true;
                isHighlightingObject = false;
                isHighlightingPalette = false;
            }
            else //We aren't highlighting anything important
            {
                line.SetPosition(1, defaultLength);
                isHighlightingPalette = false;
                isHighlightingObject = false;
                isHighlightingScroll = false;
                //currSelection = null;

            }

        }
        else //We aren't highlighting anything important
        {
            line.SetPosition(1, defaultLength);
            isHighlightingPalette = false;
            isHighlightingObject = false;
            isHighlightingScroll = false;
            //currSelection = null;
        }

    }

    public void initPaletteSelectionScaling(Vector3 initPos)
    {
       paletteSelectionInitPos = initPos;
       paletteSelectionIsScaling = true;
    }
}
