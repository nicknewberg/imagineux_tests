﻿using UnityEngine;
using System.Collections;

public class HapticFeedback : MonoBehaviour {

    public float Length, Strength;
    private bool pulsating = false;
    private int pulseCount = 0;
    public NewtonVR.NVRHand reactionHand;

    void Update()
    {
        if (pulsating && pulseCount < Strength)
        {
            reactionHand.ReactionPulse(Length);
            pulseCount++;
        }
        else if (pulsating)
        {
            pulsating = false;
            pulseCount = 0;
        }
    }

    public void Trigger()
    {
        pulsating = true;
    }
}
