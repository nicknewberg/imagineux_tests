﻿using UnityEngine;
using System.Collections;

public class RevealLeaf : MonoBehaviour {

    public GameObject player;

    public Vector3 TargetRotation; //revealed rotation (local)
    private Quaternion targetRot;
    private Quaternion initRot; //hidden rotation (local)

    private bool isRevealing; //is this Leaf revealing?

	// Use this for initialization
	void Start () {
        initRot = this.transform.localRotation;
        TargetRotation = new Vector3(402.595f, -133.692f, 217.152f);
        targetRot = Quaternion.Euler(TargetRotation);
        isRevealing = false;
	}
	
	// Update is called once per frame
	void Update () {

        float distFromPlayer = Vector3.Distance(player.transform.position, this.transform.position);

        if (distFromPlayer < 1f && distFromPlayer > 0.8f)
        {
            isRevealing = true;
        }
        else
        {
            isRevealing = false;
        }

        if (isRevealing)
        {
            this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, targetRot, Time.deltaTime * 1f);

        }else
        {
            this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, initRot, Time.deltaTime * 1f);
        }
    }
}
