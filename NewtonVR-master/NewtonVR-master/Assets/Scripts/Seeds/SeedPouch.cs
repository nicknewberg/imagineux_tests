﻿using UnityEngine;
using System.Collections;

public class SeedPouch : MonoBehaviour {

    private Rigidbody rigidbody;

    public ParticleSystem seedSystem;

    private Material initMat;
    public Material activeMat;

    private Vector3 spoutDirection; //axis of opening hole

    private bool projecting; //is this pouch launching seeds?

    public GameObject projectile;
    private GameObject thisProjectile;

    // Use this for initialization
    void Start () {
        initMat = this.GetComponent<Renderer>().material;
        spoutDirection = new Vector3(0f, 1f, 0f);
        rigidbody = this.GetComponent<Rigidbody>();
        projecting = false;
        seedSystem.enableEmission = false;
        thisProjectile = projectile;
        thisProjectile.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {

        float yVel = transform.InverseTransformDirection(rigidbody.velocity).y;

        if (yVel > 0.5f)
        {
            projecting = true;
            this.GetComponent<Renderer>().material = activeMat;
            seedSystem.enableEmission = true;
            if(!thisProjectile.activeSelf)
            {
                thisProjectile = (GameObject)GameObject.Instantiate(projectile, this.transform.position, Quaternion.identity);
                thisProjectile.GetComponent<Rigidbody>().AddForce(this.GetComponent<Rigidbody>().velocity);
            }



        }
        else
        {
            projecting = false;
            this.GetComponent<Renderer>().material = initMat;
            seedSystem.enableEmission = false;

        }

    }
}
