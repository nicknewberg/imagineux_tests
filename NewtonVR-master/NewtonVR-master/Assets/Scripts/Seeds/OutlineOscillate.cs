﻿using UnityEngine;
using System.Collections;

public class OutlineOscillate : MonoBehaviour {

	private Material outlineMat;
    public bool isActive;
	public float startVal;
	public float amplitude;
	public float frequency;

	protected void Start() {
		outlineMat = this.GetComponent<Renderer>().material;
        if (startVal == 0f)
        {
            startVal = 0.003f;
        }
        isActive = false;
        outlineMat.SetFloat("_Outline", 0f);
	}

	protected void Update() {
        if (isActive)
        {
            float distance = Mathf.Sin(Time.timeSinceLevelLoad * frequency);
            //float distance = Mathf.Sin(Time.timeSinceLevelLoad/period);
            outlineMat.SetFloat("_Outline", startVal + amplitude * distance);
        }
	}

    public void on()
    {
        this.isActive = true;
        //outlineMat.SetFloat("_Outline", 0f);
    }

    public void off()
    {
        this.isActive = false;
        outlineMat.SetFloat("_Outline", 0f);

    }

}
