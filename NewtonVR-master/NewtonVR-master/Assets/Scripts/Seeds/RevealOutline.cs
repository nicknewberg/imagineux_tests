﻿using UnityEngine;
using System.Collections;

public class RevealOutline : MonoBehaviour {

    public OutlineOscillate Outline;
    public GameObject player;
    private bool outlineOn;
    private bool interacting;
    private bool ofInterest;
    float distance;

	// Use this for initialization
	void Start () {
        outlineOn = false;
        interacting = false;
        ofInterest = true;
        player = GameObject.Find("Camera (eye)");

    }
	
	// Update is called once per frame
	void Update () {

        if (ofInterest)
        {
            distance = Vector3.Distance(player.transform.position, this.transform.position);
            if (distance < 1f && !interacting)
            {
                showOutline();
            }
            else if (interacting || outlineOn)
            {
                hideOutline();
            }
        }

    }

    public void showOutline()
    {
        Outline.on();
        outlineOn = true;
    }

    public void hideOutline()
    {
        Outline.off();
        outlineOn = false;
    }

    public void startInteracting()
    {
        interacting = true;
    }

    public void stopInteracting()
    {
        interacting = false;
    }

    public void noLongerOfInterest()
    {
        ofInterest = false;
    }
}
