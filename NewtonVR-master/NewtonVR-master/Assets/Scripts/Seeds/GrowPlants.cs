﻿using UnityEngine;
using System.Collections;

public class GrowPlants : MonoBehaviour
{
    public GameObject plant; //plant prefab we will instantiate
    private GameObject thisPlant; //the plant we instantiate on this tile
    private  bool inFocus; //is this tile in Focus
    public ParticleSystem seedSystem; //the seed particle system
    private int threshold; //how many seeds need to hit before we grow plant?
    private int seedCount; //how many seeds have hit currently
    private bool planted; //did we plant on this tile?

    // Use this for initialization
    void Start()
    {
        planted = false;
        threshold = 10;
        seedCount = 0;
        inFocus = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        print("trigger");
    }

    void OnParticleCollision(GameObject other)
    {

        //if (inFocus)
        //{
            if (!planted && seedCount >= threshold) //if we haven't planted and we've reached the threshold, plant
            {
                thisPlant = (GameObject)Instantiate(plant, this.transform.position, this.transform.localRotation);
                thisPlant.transform.rotation = this.transform.localRotation;
                thisPlant.transform.localScale = Vector3.zero;
                thisPlant.GetComponent<AnimatePlant>().Grow();
                planted = true;
            }

            seedCount++;

        //}
    }

    public void focus()
    {
        inFocus = true;
    }

    public void defocus()
    {
        inFocus = false;
    }




}