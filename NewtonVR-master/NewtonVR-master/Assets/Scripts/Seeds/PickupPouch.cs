﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class PickupPouch : NVRInteractableItem
    {

        public ParticleSystem seedSystem;

        public override void BeginInteraction(NVRHand hand)
        {
            base.BeginInteraction(hand);
            seedSystem.Emit(53);
            seedSystem.Play();

        }


    }
}

