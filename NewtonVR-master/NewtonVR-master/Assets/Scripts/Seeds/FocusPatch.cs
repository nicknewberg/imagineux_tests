﻿using UnityEngine;
using System.Collections;

public class FocusPatch : MonoBehaviour {

    private GrowPlants currGP; //current garden patch "Grow Plants"

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 fwd = transform.TransformDirection(Vector3.forward);

        RaycastHit hit;

        if (Physics.Raycast(transform.position, fwd, out hit))
        {
            if(hit.collider.tag == "GardenPatch") //set the gardenPatch that we are looking at in focus
            {
                if (currGP)
                    currGP.defocus();

                currGP = hit.collider.gameObject.GetComponent<GrowPlants>();
                currGP.focus();
            }
        }
            
    }
}
