﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class Leaf : NVRInteractableItem
    {
        private bool isRetracting; //is this leaf retracting to its original position?
        private Vector3 initPos;
        private Quaternion initRot;
        private RevealOutline Outline;

        void Awake()
        {
            base.Awake();

            isRetracting = false;
            this.initPos = this.transform.position;
            this.initRot = this.transform.rotation;
        }

        void Start()
        {
            base.Start();

            if (this.GetComponent<RevealOutline>())
            {
                Outline = this.GetComponent<RevealOutline>();
                Outline.showOutline();
            }
                
        }
        
        //// Use this for initialization
        //void Start()
        //{
        //    base.Start();

        //    isRetracting = false;
        //    this.initPos = this.transform.position;
        //    this.initRot = this.transform.rotation;
        //}

        // Update is called once per frame
        void Update()
        {
            base.Update();

            if (isRetracting)
            {

                this.transform.position = Vector3.Lerp(this.transform.position, this.initPos, Time.deltaTime * 3f);
                this.transform.rotation = Quaternion.Slerp(this.transform.rotation, this.initRot, Time.deltaTime * 3f);

                if (Vector3.Magnitude(this.transform.eulerAngles - this.initRot.eulerAngles) <= 0.01f)
                {
                    isRetracting = false;
                }
            }

        }

        public override void BeginInteraction(NVRHand hand)
        {
            base.BeginInteraction(hand);
            if (Outline)
            {
                Outline.startInteracting();
                Outline.hideOutline();
            }
            print("hide outline");

        }

        public override void EndInteraction()
        {
            base.EndInteraction();
            if (PickupTransform != null)
                Destroy(PickupTransform.gameObject);

            isRetracting = true;
            if (Outline)
            {
                Outline.noLongerOfInterest(); //kills outline
                //Outline.stopInteracting();
                //Outline.showOutline();
            }
            print("show outline");
        }
    }
}

