﻿using UnityEngine;
using System.Collections;
namespace NewtonVR
{
    public class InkSetter : MonoBehaviour
    {

        public ColorButton button;
        public GameObject inkPad;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

            if (button.ButtonDown)
            {
                inkPad.GetComponent<Renderer>().material = this.GetComponent<Renderer>().material;
            }

        }
    }
}

