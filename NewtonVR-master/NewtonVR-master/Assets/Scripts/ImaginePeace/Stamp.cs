﻿using UnityEngine;
using System.Collections;

public class Stamp : MonoBehaviour {

    public GameObject stampText; //the displayed stamp text
    public GameObject stampPrefab; //the actual prefab that will be placed as a stamp
    private bool inked;

	// Use this for initialization
	void Start () {
        inked = false;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "inkpad")
        {
            inked = true;
            this.GetComponent<Renderer>().material = other.gameObject.GetComponent<Renderer>().material;
            stampText.GetComponent<TextMesh>().color = other.GetComponent<Renderer>().material.color;
            stampPrefab.GetComponent<TextMesh>().color = other.GetComponent<Renderer>().material.color;
        }
        else if (inked)
        {
            Vector3 stampRot = this.transform.rotation.eulerAngles + new Vector3(90f, 90f, 0f);
            Instantiate(stampPrefab, this.transform.position, Quaternion.Euler(stampRot));
        }
        
    }

}
