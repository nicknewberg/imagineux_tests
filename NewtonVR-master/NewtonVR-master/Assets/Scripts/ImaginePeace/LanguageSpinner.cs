﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class LanguageSpinner : NVRLetterSpinner
    {
        private string[] words = { "IMAGINE PEACE", "IMAGINARSE LA PAZ", "IMAGINE PEACE03", "представьте себе мир", "平和を想像します" };
        public string GetStampWord()
        {
            int closest = Mathf.RoundToInt(this.transform.localEulerAngles.z / RungAngleInterval);
            if (this.transform.localEulerAngles.z < 0.3)
                closest = LETTERLIST.Length - closest;

            if (closest == 27) //hack
                closest = 0;
            if (closest == -1)
                closest = 26;

            return words[closest];
        }

    }
}

