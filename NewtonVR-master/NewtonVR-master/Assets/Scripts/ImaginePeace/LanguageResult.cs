﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class LanguageResult : MonoBehaviour
    {
        public LanguageSpinner spinner;
        public TextMesh prefabText; //text for 'spawned' stamp
        private TextMesh displayText; //display text for the stamp
    // Use this for initialization
    void Start()
        {
            displayText = this.GetComponent<TextMesh>();

        }

        // Update is called once per frame
        void Update()
        {
           
            displayText.text = spinner.GetStampWord();
            prefabText.text = displayText.text;

        }
    }
}

