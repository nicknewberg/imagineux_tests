﻿using UnityEngine;
using System.Collections;

public class CrackDoor : MonoBehaviour {

    public Animation doorAnimation;
    public GameObject doorHinge;
    private Vector3 openRotation;
    private bool opening;
    private bool canCrack;

	// Use this for initialization
	void Start () {
        opening = false;
        canCrack = true;
        openRotation = new Vector3(0f, 18f, 0f);
    }

    // Update is called once per frame
    void Update () {

        if (canCrack)
        {
            if (opening)
            {
                doorHinge.transform.localEulerAngles = Vector3.Lerp(doorHinge.transform.localEulerAngles, openRotation, 0.025f);

            }
            else
            {
                doorHinge.transform.localEulerAngles = Vector3.Lerp(doorHinge.transform.localEulerAngles, Vector3.zero, 0.025f);

            }
        }

	
	}

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Trigger");
        opening = true;
    }

    void OnTriggerExit(Collider other)
    {
        Debug.Log("EXIT");
        opening = false;
 

    }
}
