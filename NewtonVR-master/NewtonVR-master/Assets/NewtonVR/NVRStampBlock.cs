﻿using UnityEngine;
using System.Collections;

namespace NewtonVR
{
    public class NVRStampBlock : NVRInteractableItem
    {
        public override void EndInteraction()
        {
            base.EndInteraction();
            this.Rigidbody.velocity = Vector3.zero;
            this.Rigidbody.angularVelocity = Vector3.zero;
        }
    }
}